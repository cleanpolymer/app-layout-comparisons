# \<app-layout\> - Comparison Demos

This repository is a collection of pages to get you starterd with Polymer's **app-layout**, by comparing different configurations of **@Polymer/app-layout** implementations.

## Why Demo Code Comparisons?

_\*\*"... Oh, that's what they mean by_ app-toolbar!"\*\*

I couldn't figure things out quickly enough. For example, I started with app-layout code in the pwa-starter-kit, and then **_attempted to change this code to meet the needs of my own first app._** But then it got all tangled up with routing attributes, and it quickly devolved.

I was _**unable**_ to sit down and write, or even understand, the app-layout code in the normal 15 or more seconds that I normally budget for such tasks. :)

If you have used app-layout elements and APIs, you may have discovered that some of the documentation is very meaningful - but only once you understand what lingo means. That sometimes that takes some experimentation.

So that's what this is _**try different combinations that I could see already working,**_ and especially - simplified as much as possible, to remove distractions.

[Also see Template Driven Fantasies below.]

## 2 minute Youtube offered, for your speedy review

A fast scan of resources outside and inside this repository may give you some quick insights into the motives and how to use this project most expeditiously.

Try this [2 minute video](https://youtu.be/kd7rDhsTd_8).

## October 2018

This code might be obsolete in 2020 or later, when future versions of Lit, Polymer/app-layout documentation or ... become available.

## Review of links

- [app-layout on NPM](https://youtu.be/ssssss)
- [pwa-starter-kit](https://github.com/Polymer/pwa-starter-kit)
- [an example of an existing demo](http://polymerelements.github.io/app-layout/templates/getting-started/)
- [Material Design Components](https://github.com/material-components/material-components-web-components)

## How does this code compare to the app-layout demos?

- Identical copy-pasted as much as possible - see video.
- Uses LitElement, not Polymer 3
- Uses material design components, not Paper
- Simplified to remove unrelated code, as much as reasonable, focused solely on app-layout portions of code.
- Tends to be slightly more unified in approach, between different demos.
- Adds in a demo of the pwa-starter-kit layout, for comparison
- Leaves off these 3 more complex demos that are super-nifty, but a bit far afield of clean app-layout demos:
  - Shrine demo
  - Publishing demo
  - Blog demo

## Don't forget!

Implicit with the usage of these demos is toggling back and forth between desktop and phone views in your developer mode.

See video for an example.

## Issues or Pull Requests you may wish to file:

At time of initial commit, this was my first Lit code of any consequence. So there should be - _ahem_ - lots of opportunity to point out deficits. Please feel free. These include.

- CSS - which I generally suck at
- listener and drawer toggling code
- Lit code
- other

## Individual Pages, not SPA

This app will fire up with the usual ...

```
$ polymer serve
```

Then, access the code from localhost as you would any other polymer app.

... but it's not really a single page app.

Why? Because routing code sometimes gets mixed up with variables consumed in the app-layout (see app-layout demos), and then you can't do quick testing of modified app-layout code - or at least you lose some of the snappiness of that process.

## Why Bitbucket instead of Github?

Good question. More of a symbolic or experimental move, than one of substance.

See this: [Microsoft aquires Github](https://blogs.microsoft.com/blog/2018/10/26/microsoft-completes-github-acquisition/)

I have used the free version of Bitbucket for almost a decade, and I find it to be as useful as my github account, _excepting_ that people are not as familiar with it.

And unlike github, my bitbucket account lets me have free private repos. Recommended.

## Template Driven Fantasies:

Part of my motivation for these demos is my love affair with all things templatized, such as what [JHipster](https://www.jhipster.tech/) does for the SpringBoot with Angular and/or React communities, or even Vaadin tooling.

In this view of the world, choice of app-layout starting configuration would be one such templatized decision.

So this is a predecessor to such work, should I ever take these as templates, to the next level.

I've been messing around with [Yeoman](http://yeoman.io/authoring/index.html) and it seems to be pretty useful.

## File Structure for Firebase Deployment

If the you find the file structure for this code odd...

public/index
public src/...js

This file structure is set up for deployment to Firebase hosting.
You may view [this project deployed on firebase here](https://app-layout-comparisons.firebaseapp.com/)
