import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class AApp extends LitElement {
  static get properties() {
    return {
      mood: { type: String }
    };
  }

  constructor() {
    super();
    this.mood = "happy";
  }

  render() {
    return html`
        <style>
          .title {
            text-align: center;
          }
          .pink {
            --mdc-theme-on-primary: white;
            --mdc-theme-primary: #e9437a;
            --mdc-theme-on-secondary: white;
            --mdc-theme-secondary: #e9437a;
          }
          .wide {
            width: 350px;
          }
        </style>
        <h1 id='waddle'>Polymer/app-layout Implementations to Compare to Each Other</h1>
        <p>Each is named by and initially copy-pasted from a polymer demo page of the same name.</p>
        <p>Each is then simplified as much as possible to eliminate code which is not app-layout specific.</p>
        <p><strong>Don't forget to also view these in the small mobile format, in your browser's dev tools!</strong></p>
        <br><a href='/demo1.html'>Layout Demo 1</a>
        <br><a href='/demo2.html'>Layout Demo 2</a>
        <br><a href='/demo3.html'>Layout Demo 3</a>
        <br><a href='/demo4.html'>Layout Demo 4</a>
        <br><a href='/demo5.html'>Layout Demo 5</a>
        <br><a href='/demo6.html'>Layout Demo 6</a>
        <br><a href='/demo7.html'>Layout Demo 7</a>
        <br><a href='/demoPWA.html'>Layout Demo PWA Starter Kit</a>
        <br><a href='/demoLanding.html'>Layout Demo Landing Page</a>
        <br><br>
        <p>See the <a href="https://bitbucket.org/cleanpolymer/app-layout-comparisons/src/master/" >README</a> for more info, or the <a href="https://youtu.be/kd7rDhsTd_8" > 2 min video</a>, for a walkthrough of what motivated this.</p>

      `;
  }
}

window.customElements.define("a-app", AApp);
