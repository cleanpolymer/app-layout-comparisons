import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class AFive extends LitElement {
  _onButtonClick(e) {
    console.log("NOW " + Date());
  }
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    const shoppingCartListener = {
      handleEvent(e) {
        console.log("shopping cart clicked at " + Date());
      }
    };
    return html`
      <style is="custom-style">
        body {
          margin: 0;
          font-family: 'Roboto', 'Noto', sans-serif;
          background-color: #eee;
        }
        .blueHeader {
          background-color: #4285f4;
          color: #fff;
        }
        .blueHeader mwc-button {
          --mwc-button-ink-color: white;
        }
        .whiteHeader {
          font-weight: bold;
          background-color: white;
        }
        .iconItem {
          color: #666;
        }
        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }
        .drawer-content {
          margin-left: 20px;
          height: calc(100% - 80px);
          overflow: auto;
        }
    </style>
  <body>
    <app-drawer-layout>
      <app-drawer swipe-open slot="drawer">
        <app-header-layout has-scrolling-region>
          <app-header class="whiteHeader" waterfall fixed slot="header">
            <app-toolbar>
              <div main-title>Inbox</div>
            </app-toolbar>
            <div class="drawer-content">You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
          </app-header>
        </app-header-layout>
      </app-drawer>
      <app-header-layout>
        <app-header class="blueHeader" condenses reveals effects="waterfall" slot="header">
          <app-toolbar>
            <mwc-button icon="menu" drawer-toggle></mwc-button>
            <div main-title></div>
            <mwc-button icon="search"></mwc-button>
            <mwc-button icon="more-vert"></mwc-button>
          </app-toolbar>
          <app-toolbar></app-toolbar>
          <app-toolbar>
            <div spacer main-title>My Drive</div>
          </app-toolbar>
        </app-header>
        <sample-content size="10"></sample-content>
      </app-header-layout>
    </app-drawer-layout>
  </body>`;
  }
}

window.customElements.define("a-five", AFive);
