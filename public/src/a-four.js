import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class AFour extends LitElement {
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    return html`
      <style>
        body {
          margin: 0;
          font-family: 'Roboto', 'Noto', sans-serif;
          background-color: #eee;
        }

        app-header {
          background-color: #0b8043;
          color: white;

          --app-header-background-front-layer: {
            background-color: #4285f4;
          };
        }

        //don't know how to do this:
        mwc-button {
          --mwc-button-ink-color: white;
        }

        .mainHeader [main-title] {
          font-weight: 400;
          margin: 0 0 0 50px;
        }

        .mainHeader [condensed-title] {
          font-weight: 400;
          margin-left: 30px;
        }

        .mainHeader [condensed-title] i {
          font-style: normal;
          font-weight: 100;
        }

        app-toolbar.tall {
          height: 148px;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }
        .drawer-content {
          margin-left: 20px;
          height: calc(100% - 80px);
          overflow: auto;
        }
      </style>
    <body>
      <app-drawer-layout>
        <app-drawer swipe-open slot="drawer">
          <app-toolbar class="navToolbar">Menu:</app-toolbar>
          <div class="drawer-content">
          <br>You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
        </app-drawer>
        <app-header-layout>
          <app-header class="mainHeader" condenses fixed effects="waterfall" slot="header">
            <!-- try either this one or the one below
            <app-toolbar>
              <mwc-button icon="menu" drawer-toggle></mwc-button>
              <h4 condensed-title>Contacts</i></h4>
              <mwc-button icon="search"></mwc-button>
            </app-toolbar> -->
            <app-toolbar class="tall">
              <mwc-button icon="menu" drawer-toggle></mwc-button>
              <h1 main-title>Contacts</h1>
              <mwc-button icon="search"></mwc-button>
            </app-toolbar>
          </app-header>
          <sample-content size="10"></sample-content>
        </app-header-layout>
      </app-drawer-layout>
    </body>`;
  }
}

window.customElements.define("a-four", AFour);
