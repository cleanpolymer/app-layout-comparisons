import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class ALanding extends LitElement {
  _onButtonClick(e) {
    console.log("NOW " + Date());
  }
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    const shoppingCartListener = {
      handleEvent(e) {
        console.log("shopping cart clicked at " + Date());
      }
    };
    return html`hi
      <style>
          a {
        text-decoration: none;
        font-size: inherit;
        color: inherit;
      }

      .toolbar {
        /* display: flex;
        flex-direction: row; */
        background-color: rgba(255, 255, 255, 0.95);
      }

      .tabs {
        height: 100%;
        display: flex;
        flex-direction: row;
      }

      .tabs > a {
        /* display: flex;
        flex-direction: column; */
        margin: 12px 16px 12px;
        border-bottom: 1px solid #222;
      }

      header {
        /* display: flex;
        flex-direction: column; */
        height: calc(100vh - 64px);
        padding: 0 16px;
        background-image: url('//app-layout-assets.appspot.com/assets/landing-page/glass_explorer_food_2.png');
        background-repeat: no-repeat;
        background-size: cover;
        color: white;
        text-align: center;
      }

      header > h2 {
        font-size: 56px;
        font-weight: 300;
        margin: 0;
      }

      header > p {
        font-size: 32px;
      }

      section {
        padding: 88px 16px;
      }

      .container {
        /* display: flex;
        flex-direction: row; */
        max-width: 800px;
        margin: 0 auto;
      }

      /* .container > * {
        display: flex;
        flex-direction: row;
      } */

      .container img {
        max-width: 100%;
        max-height: 100%;
      }

      .container h3 {
        font-size: 32px;
        font-weight: 300;
        margin: 24px 0;
      }

      .container p {
        line-height: 1.5;
      }

      /* @media (max-width: 600px) {
        .container {
          display: flex;
          flex-direction: column;
        }
      } */

    </style>
<body>
    <app-header-layout>
      <app-header reveals effects="waterfall" slot="header">
        <app-toolbar class="toolbar">
          <div class="tabs">
            <a href="#about">About</a>
            <a href="#services">Services</a>
            <a href="#contact">Contact</a>
          </div>
        </app-toolbar>
      </app-header>
      <header>
        <h2>Welcome To Our Landing Page</h2>
        <p>Made with App Layout Elements</p>
      </header>
      <section id="about">
        <div class="container">
          <div>
            <img src="//app-layout-assets.appspot.com/assets/landing-page/berries.jpg">
          </div>
          <div>
            <h3>About</h3>
            <p>Iron-flex-layout was removed from this code because has been deprecated in favor of CSS Flex but did not figure out how to implement it</p>
            <p>Vertically centering text inside the Welcome To Our Landing Page was also not implemented</p>
          </div>
        </div>
      </section>
      <section id="services">
        <div class="container">
          <div>
            <img src="//app-layout-assets.appspot.com/assets/landing-page/tomato.jpg">
          </div>
          <div>
            <h3>Services</h3>
            <p>Lorem ipsum dolor sit amet, per in nusquam nominavi periculis, sit elit oportere ea. Lorem ipsum dolor sit amet, per in nusquam nominavi periculis, sit elit oportere ea.</p>
            <p>Convenire definiebas scriptorem eu cum. Sit dolor dicunt consectetuer no. Convenire definiebas scriptorem eu cum. Sit dolor dicunt consectetuer no.</p>
          </div>
        </div>
      </section>
      <section id="contact">
        <div class="container">
          <div>
            <img src="//app-layout-assets.appspot.com/assets/landing-page/red-onion.jpg">
          </div>
          <div>
            <h3>Contact</h3>
            <p>Lorem ipsum dolor sit amet, per in nusquam nominavi periculis, sit elit oportere ea. Lorem ipsum dolor sit amet, per in nusquam nominavi periculis, sit elit oportere ea.</p>
            <p>Convenire definiebas scriptorem eu cum. Sit dolor dicunt consectetuer no. Convenire definiebas scriptorem eu cum. Sit dolor dicunt consectetuer no.</p>
          </div>
        </div>
      </section>
    </app-header-layout>
      </body>
      `;
  }
}

window.customElements.define("a-landing", ALanding);
