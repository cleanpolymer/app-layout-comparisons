import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class AOne extends LitElement {
  _onButtonClick(e) {
    console.log("NOW " + Date());
  }
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    const shoppingCartListener = {
      handleEvent(e) {
        console.log("shopping cart clicked at " + Date());
      }
    };
    return html`
        <style>

    body {
      margin: 0;
      font-family: 'Roboto', 'Noto', sans-serif;
      background-color: #eee;
    }

    #startDrawer {
      --app-drawer-content-container: {
        box-shadow: 1px 0 2px 1px rgba(0,0,0,0.18);
      }
    }

    #endDrawer {
      --app-drawer-content-container: {
        box-shadow: -1px 0 2px 1px rgba(0,0,0,0.18);
      }
    }

    app-header {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      background-color: #4285f4;
      color: #fff;
    }

    app-header paper-icon-button {
      --paper-icon-button-ink-color: white;
    }

    sample-content {
      padding-top: 192px;
    }
    .drawer-content {
      margin-left: 20px;
      height: calc(100% - 80px);
      overflow: auto;
    }

  </style>
  <body>
    <app-header condenses reveals effects="waterfall">
      <app-toolbar>
        <mwc-button  @click=${menuListener} icon="menu"></mwc-button>
        <div main-title></div>
        <mwc-button  @click=${shoppingCartListener} icon="shopping_cart"></mwc-button>
      </app-toolbar>
      <app-toolbar>
        <div spacer main-title>My App</div>
      </app-toolbar>
    </app-header>
    <app-drawer id="startDrawer" align="start">
        <app-toolbar>App name</app-toolbar>
        <div class="drawer-content">You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
    </app-drawer>
    <app-drawer id="endDrawer" align="end"></app-drawer>
    <sample-content size="100"></sample-content>
  </body>
      `;
  }
}

window.customElements.define("a-one", AOne);
