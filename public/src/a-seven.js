import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class ASeven extends LitElement {
  _onButtonClick(e) {
    console.log("NOW " + Date());
  }
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    const shoppingCartListener = {
      handleEvent(e) {
        console.log("shopping cart clicked at " + Date());
      }
    };
    return html`
      <style>
        body {
          margin: 0;
          font-family: 'Roboto', 'Noto', sans-serif;
          background-color: #eee;
        }
        app-header {
          background-color: #4285f4;
          color: #fff;
        }
        app-header mwc-button {
          --paper-icon-button-ink-color: white;
        }
        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }
        app-header app-toolbar mwc-button {
          --mwc-button-ink-color: white;
        }
        .drawer-content {
          margin-left: 20px;
          text-align: left;
          height: calc(100% - 80px);
          overflow: auto;
        }
      </style>
      <body>
        <app-drawer-layout fullbleed>
          <app-header-layout fullbleed>
            <app-header fixed effects="waterfall" slot="header">
              <app-toolbar>
                <div spacer main-title>My App</div>
                      <mwc-button  @click=${menuListener} icon="menu" drawer-toggle></mwc-button>
              </app-toolbar>
            </app-header>
            <sample-content size="100"></sample-content>
          </app-header-layout>
          <app-drawer align="end" slot="drawer">
            <app-toolbar>App Menu</app-toolbar>
            <div class="drawer-content">You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
          </app-drawer>
        </app-drawer-layout>
      </body>
      `;
  }
}

window.customElements.define("a-seven", ASeven);
