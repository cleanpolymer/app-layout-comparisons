import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class ASix extends LitElement {
  render() {
    const menuListener = {
      handleEvent(e) {
        // this.renderRoot.querySelector("#startDrawer").toggle();
        console.log(
          "HI BUDDY\n" + this.renderRoot.querySelector("#startDrawer")
        );
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    return html`
        <style>


    body {
      margin: 0;
      font-family: 'Roboto', 'Noto', sans-serif;
      background-color: #eee;
    }

    app-header {
      background-color: #4285f4;
      color: #fff;
    }

    app-header mwc-button {
      --mwc-button-ink-color: #fff;
    }

    app-drawer-layout {
      --app-drawer-layout-content-transition: margin 0.2s;
    }

    app-drawer {
      --app-drawer-content-container: {
        background-color: #eee;
      }
    }

    .drawer-content {
      margin-top: 80px;
      margin-left: 20px;
      height: calc(100% - 80px);
      overflow: auto;
    }

  </style>
  <body>
    <app-header-layout>
      <app-header fixed effects="waterfall" slot="header">
        <app-toolbar>
          <mwc-button id="toggle" icon="menu"  drawer-toggle @click=${menuListener} ></mwc-button>
          <div main-title>App Title Here</div>
        </app-toolbar>
      </app-header>
      <app-drawer-layout id="drawerLayout">

    <app-drawer id="startDrawer" align="start">
      <div class="drawer-content"><h4>Menu:</h4>
      <br>You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
    </app-drawer>
    <app-drawer id="endDrawer" align="end"></app-drawer>

        <sample-content size="10"></sample-content>
      </app-drawer-layout>
    </app-header-layout>
  </body>
      `;
  }
}

window.customElements.define("a-six", ASix);
