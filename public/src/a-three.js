import {
  LitElement,
  customElement,
  property,
  html
} from "../node_modules/@polymer/lit-element";
import "../node_modules/@material/mwc-button";
import "../node_modules/@material/mwc-icon";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
// Import effects before app-header.
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "./sample-content.js";

class AThree extends LitElement {
  _onButtonClick(e) {
    console.log("NOW " + Date());
  }
  render() {
    const menuListener = {
      handleEvent(e) {
        this.renderRoot.querySelector("#startDrawer").toggle();
      }
    };
    const shoppingCartListener = {
      handleEvent(e) {
        console.log("shopping cart clicked at " + Date());
      }
    };
    return html`
        <style>


    body {
      margin: 0;
      font-family: 'Roboto', 'Noto', sans-serif;
      background-color: #eee;
    }

    app-header {
      background-color: #4285f4;
      color: #fff;
    }

    app-drawer-layout:not([narrow]) [drawer-toggle] {
      display: none;
    }
    app-header app-toolbar mwc-button {
      --mwc-button-ink-color: white;
    }
    .drawer-content {
      margin-left: 20px;
      height: calc(100% - 80px);
      overflow: auto;
    }
  </style>
  <body>
    <app-drawer-layout fullbleed>
      <app-drawer id='startDrawer' slot="drawer">
        <app-toolbar>Menu</app-toolbar>
          <div class="drawer-content">
          <br>You<br>would<br>code<br>a<br>properly<br>styled<br>menu<br>here!</div>
      </app-drawer>
      <app-header-layout has-scrolling-region>
        <app-header condenses reveals effects="waterfall" slot="header">
          <app-toolbar>
            <mwc-button  @click=${menuListener} icon="menu"></mwc-button>
          </app-toolbar>
          <app-toolbar></app-toolbar>
          <app-toolbar>
            <div spacer main-title>My Drive</div>
          </app-toolbar>
        </app-header>
        <p>This demo3 appears to be broken, or at least the functionality shown doesn't seem desirable.</p>
        <p> To see an even more broken demo3, look no further than the demo3 that this code is copy-pasted from, which is the code from <a href="https://github.com/PolymerElements/app-layout/blob/master/demo/demo3.html">app-layout/demo/demo3.html</a></p>
        <p>So main thing is if you know how to restore either to the actual intent of the demo, please let me know.</p>
        <sample-content size="10"></sample-content>
      </app-header-layout>
    </app-drawer-layout>
  </body>
      `;
  }
}

window.customElements.define("a-three", AThree);
